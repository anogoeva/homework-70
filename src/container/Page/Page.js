import React, {useEffect} from 'react';
import Product from "../../components/Products/Product";
import {useDispatch, useSelector} from "react-redux";
import {addProduct, fetchProducts} from "../../store/actions";
import Cart from "../../components/Cart/Cart";
import "./Page.css";
import {nanoid} from "nanoid";

const Page = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.productReducer.products);

    useEffect(() => {
        dispatch(fetchProducts());
    }, [dispatch]);

    const addProductHandler = product => {
        dispatch(addProduct(product));
    };

    return (
        <div className="Page">
            <div className="ProductsControl">
                {products ?
                    Object.values(products).map(product => {
                        return (
                            <Product
                                key={nanoid()}
                                name={product.name}
                                img={product.img_url}
                                price={product.price}
                                add={() => addProductHandler(product)}
                            />
                        );
                    })
                    :
                    ''}
            </div>
            <div className="Cart"><Cart key={nanoid()}/></div>
        </div>
    );
};

export default Page;