import {
    ADD_PRODUCT,
    FETCH_PRODUCTS_FAILURE,
    FETCH_PRODUCTS_REQUEST,
    FETCH_PRODUCTS_SUCCESS,
    REMOVE_PRODUCT,
    SET_MODAL_OPEN,
} from "../actions";

import {BASE_PRICE, PRODUCTS} from "../../constants";

const initialState = {
    fetchProductsLoading: false,
    products: null,
    order: {
        plov: {
            count: 0,
            price: 0
        },
        salad: {
            count: 0,
            price: 0
        },
        bread: {
            count: 0,
            price: 0
        },
    },
    totalPrice: BASE_PRICE,
    showPurchaseModal: false,

};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case REMOVE_PRODUCT:
            let tmp_price_r = 0;
            if (action.payload === 'bread') {
                tmp_price_r = 20;
            } else if (action.payload === 'salad') {
                tmp_price_r = 100;
            } else if (action.payload === 'plov') {
                tmp_price_r = 220;
            }
            if (state.products[action.payload].count <= 0) {
                return state;
            }
            return {
                ...state,
                order: {
                    ...state.order,
                    [action.payload]: {
                        ...state.order[action.payload].count,
                        ...state.order[action.payload].price,
                        count: state.order[action.payload].count - 1,
                        price: state.order[action.payload].price - tmp_price_r,
                    }
                },
                totalPrice: state.totalPrice - PRODUCTS[action.payload].price
            };
        case ADD_PRODUCT:
            let tmp_price = 0;
            if (action.payload.name === 'bread') {
                tmp_price = 20;
            } else if (action.payload.name === 'salad') {
                tmp_price = 100;
            } else if (action.payload.name === 'plov') {
                tmp_price = 220;
            }
            return {
                ...state,
                order: {
                    ...state.order,
                    [action.payload.name]: {
                        ...state.order[action.payload.name].count,
                        ...state.order[action.payload.name].price,
                        count: state.order[action.payload.name].count + 1,
                        price: state.order[action.payload.name].price + tmp_price,
                    }
                },
                totalPrice: state.totalPrice + PRODUCTS[action.payload.name].price,
            };
        case SET_MODAL_OPEN:
            return {
                ...state, showPurchaseModal: action.payload
            };
        case FETCH_PRODUCTS_REQUEST:
            return {...state, fetchProductsLoading: true};
        case FETCH_PRODUCTS_SUCCESS:
            return {...state, fetchProductsLoading: false, products: action.payload};
        case FETCH_PRODUCTS_FAILURE:
            return {...state, fetchProductsLoading: false};
        default:
            return state;
    }
};

export default reducer;