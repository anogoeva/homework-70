import axios from "axios";
import {apiURL} from "../config";

export const FETCH_PRODUCTS_REQUEST = 'FETCH_PRODUCTS_REQUEST';
export const FETCH_PRODUCTS_SUCCESS = 'FETCH_PRODUCTS_SUCCESS';
export const FETCH_PRODUCTS_FAILURE = 'FETCH_PRODUCTS_FAILURE';

export const SEND_PRODUCTS_REQUEST = 'SEND_PRODUCTS_REQUEST';
export const SEND_PRODUCTS_SUCCESS = 'SEND_PRODUCTS_SUCCESS';
export const SEND_PRODUCTS_FAILURE = 'SEND_PRODUCTS_FAILURE';

export const ADD_PRODUCT = 'ADD_PRODUCT';
export const REMOVE_PRODUCT = 'REMOVE_PRODUCT';
export const SET_MODAL_OPEN = 'SET_MODAL_OPEN';

export const addProduct = product => ({type: ADD_PRODUCT, payload: product});
export const removeProduct = (key) => ({type: REMOVE_PRODUCT, payload: key});
export const setModalOpen = isOpen => ({type: SET_MODAL_OPEN, payload: isOpen});


export const fetchProductsRequest = () => ({type: FETCH_PRODUCTS_REQUEST});
export const fetchProductsSuccess = products => ({type: FETCH_PRODUCTS_SUCCESS, payload: products});
export const fetchProductsFailure = () => ({type: FETCH_PRODUCTS_FAILURE});

export const sendProductsRequest = () => ({type: SEND_PRODUCTS_REQUEST});
export const sendProductsSuccess = products => ({type: SEND_PRODUCTS_SUCCESS, payload: products});
export const sendProductsFailure = () => ({type: SEND_PRODUCTS_FAILURE});

export const fetchProducts = () => {
    return async dispatch => {
        try {
            dispatch(fetchProductsRequest());
            const response = await axios.get(apiURL + '/products.json');
            dispatch(fetchProductsSuccess(response.data));
        } catch (e) {
            dispatch(fetchProductsFailure());
        }
    }
};

export const sendDataToFireBase = (formData) => {
    return async dispatch => {
        try {
            dispatch(sendProductsRequest());
            const response = await axios.post(apiURL + '/orders.json', formData);
            dispatch(sendProductsSuccess(response.data));
        } catch (e) {
            dispatch(sendProductsFailure());
        } finally {
            window.location.reload();
        }
    }
};