import './App.css';
import Page from "./container/Page/Page";

const App = () => (
    <div className="App">
        <Page/>
    </div>
);

export default App;
