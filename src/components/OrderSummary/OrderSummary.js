import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {sendDataToFireBase} from "../../store/actions";

const OrderSummary = () => {
    const dispatch = useDispatch();
    const sendOrder = [];
    const order = useSelector(state => state.productReducer.order);
    Object.entries(order).map(([key, value], i) => {
        const obj = {};
        obj[key] = value.count;
        return sendOrder.push(obj);
    });
    const [formData, setFormData] = useState({});
    const onInputTextareaChange = e => {
        const {name, value} = e.target;
        setFormData(prev => ({
            ...prev,
            [name]: value,
            orders: sendOrder,
        }));
    };
    const sendData = () => {
        dispatch(sendDataToFireBase(formData));
    };
    return (
        <>
            <form action="">
                <p>
                    <label htmlFor="name">ФИО: </label>
                    <input type="text" id="name" name="name" onChange={onInputTextareaChange}/>
                </p>
                <p>
                    <label htmlFor="address">Адрес: </label>
                    <input type="text" id="address" name="address" onChange={onInputTextareaChange}/>
                </p>
                <p>
                    <label htmlFor="phone">Телефон: </label>
                    <input type="text" id="phone" name="phone" onChange={onInputTextareaChange}/>
                </p>
                <button onClick={sendData} type="button">Создать</button>
            </form>
        </>
    );
};

export default OrderSummary;