import React from 'react';

const Product = props => {
    let name = null;

    if (props.name === 'bread') {
        name = 'Хлеб'
    } else if (props.name === 'salad') {
        name = 'Салат'
    } else {
        name = 'Плов'
    }

    return (
        <div className="ProductsControl">
            <div className="ProductsName">{name}</div>
            <p>Стоимость - {props.price} сом</p>
            <img src={props.img} alt="product" width="150px" height="100px"/>
            <p>
                <button className="ProductsAdd" onClick={props.add}>Add to cart</button>
            </p>
        </div>
    );
};

export default Product;