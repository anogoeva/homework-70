import React, {useMemo} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {BASE_PRICE} from "../../constants";
import Modal from "../UI/Modal/Modal";
import OrderSummary from "../OrderSummary/OrderSummary";
import {removeProduct, setModalOpen} from "../../store/actions";

const Cart = () => {
    const dispatch = useDispatch();
    const products = useSelector(state => state.productReducer.order);
    const totalPrice = useSelector(state => state.productReducer.totalPrice);
    const showPurchaseModal = useSelector(state => state.productReducer.showPurchaseModal);
    const purchaseCancelHandler = () => {
        dispatch(setModalOpen(false));
    };
    const purchaseHandler = () => {
        dispatch(setModalOpen(true));
    };
    const purchasable = useMemo(() => {
        const totalProducts = Object.keys(products)
            .map(name => products[name].count)
            .reduce((sum, el) => sum + el, 0);
        return totalProducts > 0;
    }, [products]);
    const removeProductHandler = (key) => {
        dispatch(removeProduct(key));
    };

    return (
        <div>
            {Object.entries(products).map(([key, value], i) => {
                    if (value.count > 0) {
                        let product;
                        if (key === 'bread') {
                            product = 'Хлеб'
                        } else if (key === 'salad') {
                            product = 'Салат'
                        } else if (key === 'plov') {
                            product = 'Плов'
                        }
                        return <p key={i} onClick={() => removeProductHandler(key)}>{product} * {value.count} ---
                            Сумма: {value.price} KGS</p>
                    }
                    return null;
                }
            )
            }
            <div>
                <Modal
                    show={showPurchaseModal}
                    close={purchaseCancelHandler}
                >
                    <OrderSummary
                        products={products}
                        onCancel={purchaseCancelHandler}
                    />
                </Modal>
                <p>Доставка: {BASE_PRICE} KGS</p>
                <p>Итого: {totalPrice} KGS</p>
                <button className="OrderButton" onClick={purchaseHandler} disabled={!purchasable}>Place order</button>
            </div>
        </div>
    );
};

export default Cart;